# Repo con le dispense per le Olimpiadi di Fisica
Dopo molto tempo ho deciso di pubblicare il sorgente del lavoro. Sfruttatelo per imparare a scrivere qualcosa in LaTeX.

# Link al file compilato

Potete trovare il PDF già compilato [qui](https://orsobruno96.gitlab.io/dispenseOlimpiadi/dispense_olimpiadi.pdf).


# Istruzioni di compilazione

## Linux e Mac
Installate i pacchetti `texlive-full` e `latexmk`. A questo punto sfruttate il comodo file `utils`

```bash
./utils build
```

che compilerà il sorgente il numero giusto di volte e ricompilerà ogni volta che fate una modifica.

## Windows
Condoglianze.
