# Come contribuire
Potete aggiungere liberamente le soluzioni dei problemi che mancano. Modificate il file TeX opportuno e fate una merge request. La CI di GitLab dovrebbe controllare da sola che il TeX compili e che non ci siano conflitti. Io leggerò la soluzione e se è corretta la mergerò volentieri.


# Lavori da fare
Per esempio nella cartella `problemi_da_aggiungere` ci sono diversi problemi che non sono ancora stati `TeX`ati e che si potrebbero aggiungere in quanto interessanti. Potreste per esempio `TeX`arne uno e cancellare il file pdf una volta messo dentro il sorgente principale.

Un'altra cosa che manca e che potete fare è scrivere le soluzioni mancanti dei problemi o proporne di nuovi.


## Quali file modificare

### Spiegazione degli environment
Il rapporto problema-soluzione è così strutturato:

- Ogni problema deve far parte di una categoria, per esempio "meccanica" attraverso un environment
- Dentro l'environment del problema viene scritta la soluzione dentro il suo environment
- Nel file `src/soluzioni/soluzioni.tex` c'è solo la lista degli Hint e la lista delle categorie di problemi di cui stampare la soluzione.
- Vengono generate automaticamente la ref per il problema e la soluzione a partire dal `nome_della_ref`
- Esempio minimale più esplicativo:


```LaTeX

Nel file di meccanica:

\begin{problemcathegory}[meccanica]

  \begin{problemwithsol}{Nome del problema}{nome_della_ref}\small\score{2}{5}
    \[ \vec a = \vec F / m \]
    Vero o falso?
    
    \begin{solution}
      L'effetto Aharonov-Bohm è una brutta bestia
    \end{solution}
  \end{problemwithsol}

\end{problemcathegory}




Nel file delle soluzioni:
\printanswers{meccanica}

```

### Conclusioni
In sostanza aggiungete le soluzioni dentro il `\begin{problemwithsol}`, nel file opportuno.


Se dovete aggiungere disegni al problema o alla soluzione siete i benvenuti. Mettetele nella cartella `immagini`, possibilmente in una sottocartella, che abbia un nome senza spazi e caratteri speciali. Il formato migliore per le immagini è il `pdf` perché è vettoriale, ma in realtà va bene tutto quello che compila.


# Buone norme per il LaTeX
Per evitare di farmi modificare a mano delle imprecisioni, suggerisco le seguenti buone norme per scrivere un testo bello e ben leggibile.

- Se dovete indicare un pedice come `_{tot}` o `_{int}`, insomma qualcosa di discorsivo e non un indice, usate `\ped{int}`.
- Se dovete indicare dei differenziali, usate la macro `\dd` per una `d` dritta, `\d` per una derivata parziale. Per esempio una derivata si scrive

```
\frac{\dd f}{\dd t}
```
- Usate le comode macro `\dsum`, `\dint` e `\dfrac` che semplicemente mettono un `displaystyle` prima di `\sum`, `\int` e `\frac`.
- Non usate mai i tre puntini di sospensione `...` ma usate invece `\ldots`.
- Cercate di usare dei nomi per le references più sensati di quelli che ho usato io. Nessuno spazio, che danno sempre dispiaceri, nomi esplicativi. Per esempio `\label{fig:disegno_problema_magneti_solenoide}`, non `\label{problema magneti}`.
- Non usate mai il doppio dollaro `$$` per iniziare e finire le formule. Usate piuttosto `\[` e `\]` se non volete numerare la formula, `\begin{equation}` e `\end{equation}` per una formula numerata.
